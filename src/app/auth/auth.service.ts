import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private _isLogin = false;
  private _isAdmin = false;

  get loginState() {
    return this._isLogin;
  }

  getAdminState() {
    return this._isAdmin;
  }

  constructor() {}

  isAdmin() {
    this._isAdmin = true;
  }

  isNotAdmin() {
    this._isAdmin = false;
  }

  login() {
    this._isLogin = true;
  }

  logout() {
    this._isLogin = false;
  }
}
