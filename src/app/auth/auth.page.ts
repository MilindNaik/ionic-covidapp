import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { LoadingController, AlertController } from '@ionic/angular';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.page.html',
  styleUrls: ['./auth.page.scss'],
})
export class AuthPage implements OnInit {
  isLoading = false;
  constructor(
    private authService: AuthService,
    private router: Router,
    private loadingController: LoadingController,
    private alertController: AlertController
  ) {}

  ngOnInit() {}
  onSubmit(form: NgForm) {
    console.log(form.form.value);
    this.isLoading = true;
    this.loadingController
      .create({
        keyboardClose: true,
        message: 'Logging in...',
      })
      .then((loadingEl) => {
        loadingEl.present();
        if (
          form.form.value.username === 'admin' &&
          form.form.value.password === 'admin'
        ) {
          this.authService.login();
          this.authService.isAdmin();
          this.router.navigateByUrl('/home');
          loadingEl.dismiss();
          form.reset();
        } else if (
          form.form.value.username === 'user' &&
          form.form.value.password === 'user'
        ) {
          this.authService.login();
          this.authService.isNotAdmin();
          this.router.navigateByUrl('/home');
          loadingEl.dismiss();
          form.reset();
        } else {
          loadingEl.dismiss();
          this.alertController
            .create({
              header: 'Authentication failed',
              message: 'invalid credentials',
              buttons: ['Okay'],
            })
            .then((alertEl) => {
              alertEl.present();
              form.reset();
            });
        }
      });
  }
}
