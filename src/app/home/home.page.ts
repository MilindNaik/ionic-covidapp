import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AlertController, LoadingController } from '@ionic/angular';
import { PatientRecordsService } from '../patient-records/patient-records.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  constructor(
    private patientService: PatientRecordsService,
    private loadingController: LoadingController,
    private alertController: AlertController
  ) {}

  onSubmit(form: NgForm) {
    this.loadingController
      .create({
        keyboardClose: true,
        message: 'Adding Patient Details',
      })
      .then((loadingEl) => {
        loadingEl.present();
        this.patientService
          .addPatient(form.form.value.Name, form.form.value.AadharNumber)
          .subscribe(() => {
            console.log('Patient added');
          });
        loadingEl.dismiss();
        form.reset();
      });
    this.alertController
      .create({
        header: 'Success',
        message: 'Patient Details Added',
        buttons: ['Okay'],
      })
      .then((alertEl) => {
        alertEl.present();
      });
  }
}
