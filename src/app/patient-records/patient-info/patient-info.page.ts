import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { LoadingController, AlertController } from '@ionic/angular';
import { AuthService } from 'src/app/auth/auth.service';
import { PatientRecordsService } from '../patient-records.service';

@Component({
  selector: 'app-patient-info',
  templateUrl: './patient-info.page.html',
  styleUrls: ['./patient-info.page.scss'],
})
export class PatientInfoPage implements OnInit {
  isAdmin = false;
  pageColor: any;
  loadedPatientRecord: any;
  covidResult;
  isLoading = false;
  reports = ['Positive', 'Negative'];
  constructor(
    private activatedRoute: ActivatedRoute,
    private patientService: PatientRecordsService,
    private loadingController: LoadingController,
    private alertController: AlertController,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.isAdmin = this.authService.getAdminState();
    this.activatedRoute.paramMap.subscribe((paramMap) => {
      if (!paramMap.has('patientId')) {
        return;
      }
      const patientRecordId = paramMap.get('patientId');
      this.isLoading = true;
      this.patientService
        .getSinglePatientRecord(patientRecordId)
        .subscribe((data) => {
          this.loadedPatientRecord = data;
          console.log('Loaded Patient record', this.loadedPatientRecord);
          this.covidResult = this.loadedPatientRecord.CovidResult;
          if (this.covidResult == 'Positive') {
            this.pageColor = 'rgb(255, 56, 56)';
          }
          if (this.covidResult == 'Negative') {
            this.pageColor = 'rgb(96, 255, 56)';
          }
          this.isLoading = false;
          console.log('CovidResult', this.covidResult);
        });
    });
  }

  updateCovidResult(form: NgForm) {
    this.covidResult = form.form.value.CovidResult;
    if (this.covidResult == 'Positive') {
      this.pageColor = 'rgb(255, 56, 56)';
    }
    if (this.covidResult == 'Negative') {
      this.pageColor = 'rgb(96, 255, 56)';
    }
    console.log('Updated Covid Result', this.covidResult);
  }

  onSubmit(id: any, form: NgForm) {
    console.log(id, form.form.value);
    this.loadingController
      .create({
        keyboardClose: true,
        message: 'Editing Data',
      })
      .then((loadingEl) => {
        loadingEl.present();
        this.patientService
          .editPatient(
            id,
            form.form.value.Name,
            form.form.value.AadharNumber,
            form.form.value.CovidResult
          )
          .subscribe(() => {
            console.log('Patient Data Changed');
          });
        loadingEl.dismiss();
        form.reset();
      });
    this.alertController
      .create({
        header: 'Success',
        message: 'Patient Data Changed',
        buttons: ['Okay'],
      })
      .then((alertEl) => {
        alertEl.present();
      });
  }
}
