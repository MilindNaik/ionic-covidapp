import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth/auth.service';
import { PatientRecordsService } from './patient-records.service';

@Component({
  selector: 'app-patient-records',
  templateUrl: './patient-records.page.html',
  styleUrls: ['./patient-records.page.scss'],
})
export class PatientRecordsPage implements OnInit {
  isLoading = false;
  isAdmin = false;
  fetchedPatientRecords: any;

  constructor(
    private patientService: PatientRecordsService,
    private authService: AuthService,
    private router: Router
  ) {}

  ngOnInit() {}
  ionViewWillEnter() {
    this.isAdmin = this.authService.getAdminState();
    this.isLoading = true;
    this.patientService.getPatientRecords().subscribe((data) => {
      this.fetchedPatientRecords = data;
      console.log('data from .net', this.fetchedPatientRecords);
      this.isLoading = false;
    });
  }

  viewPatientRecord(id: any) {
    this.router.navigateByUrl(`patient-records/${id}`);
  }
}
