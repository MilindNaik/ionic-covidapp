export class IPatientModel {
  constructor(
    public Name: string,
    public AadharNumber: string,
    public CovidResult?: string
  ) {}
}
