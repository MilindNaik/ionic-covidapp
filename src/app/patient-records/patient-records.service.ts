import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IPatientModel } from './patient-record.model';

@Injectable({
  providedIn: 'root',
})
export class PatientRecordsService {
  constructor(private http: HttpClient) {}

  getPatientRecords() {
    return this.http.get(`https://localhost:44358/api/patients`);
  }

  getSinglePatientRecord(id: any) {
    return this.http.get(`https://localhost:44358/api/patients/${id}`);
  }

  addPatient(Name: string, AadharNumber: string) {
    const newRecord = new IPatientModel(Name, AadharNumber);
    return this.http.post<any>(`https://localhost:44358/api/patients`, {
      ...newRecord,
    });
  }

  editPatient(
    id: any,
    Name: string,
    AadharNumber: string,
    CovidResult?: string
  ) {
    const editedRecord = new IPatientModel(Name, AadharNumber, CovidResult);
    return this.http.put<any>(`https://localhost:44358/api/patients/${id}`, {
      ...editedRecord,
    });
  }
}
